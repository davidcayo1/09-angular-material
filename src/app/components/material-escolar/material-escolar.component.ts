import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-material-escolar',
  templateUrl: './material-escolar.component.html',
  styleUrls: ['./material-escolar.component.css']
})
export class MaterialEscolarComponent implements OnInit {

  form!: FormGroup;

  listaDeMaterial: string[]= [];
  listaDeMaterialCadena!: string;

  constructor(
    private fb: FormBuilder

  ) { 
    this.crearFormulario();
  }

  ngOnInit(): void {
  }

  get utilesEscolares() {
    return this.form.get('utiles') as FormArray;
  }

  // Para verificar si los carácteres son validos
  get nombreNoValido() {
    // si al obtener nombre este es invalido y es touched aplicaremos una clase de css en el html
    // el signo '?' de interrogación indica que sea opcional ya que al principio se muestra una casilla vacia ''
    return (
      this.form.get('nombre')?.invalid && this.form.get('nombre')?.touched
    );
  }

  crearFormulario(): void {

    this.form = this.fb.group({
      utiles: this.fb.array([])
    })

    this.utilesEscolares.push(this.fb.group({
      aceptar: [null],
      nombre: [null, [Validators.pattern(/^[0-9a-zA-zñÑ\s]+$/)]]
    }))
  }


  agregarUtil(): void {
    const nuevoUtil = this.fb.group({
      aceptar: [null],
      nombre: [null, [Validators.pattern(/^[0-9a-zA-zñÑ\s]+$/)]]
    })
    // agregamos un nuevo util al inicio de array
    this.utilesEscolares.insert(0, nuevoUtil);
  }

  eliminarUtil(id: number): void {
    this.utilesEscolares.removeAt(id)
  }

  eliminarUtiles(): void {
    this.listaDeMaterial = [];
    this.utilesEscolares.clear();
  }

  limpiarCajas(): void { 

    for (let i = 0; i < this.utilesEscolares.length; i++) {
      // En cada posicion del array devolvemos su valor a ''
      this.utilesEscolares.at(i).get('nombre')?.setValue(null);   
      this.utilesEscolares.at(i).get('aceptar')?.setValue(null);


      // limpiamos a lista de utiles escolares
      this.listaDeMaterial = [];
      this.listaDeMaterialCadena = this.listaDeMaterialCadena.toString();
    }
    // reseteamos toda la caja de utiles,el signo '?' pregunta si es que existe algun valor
    this.form.get('utiles')?.reset;

  }


 guardarCajas(): void {

    this.listaDeMaterial = [];

    for (let i = 0; i < this.utilesEscolares.length; i++) { 
      
      if (this.utilesEscolares.at(i).get('aceptar')?.value === true 
         && this.utilesEscolares.at(i).get('nombre')?.value != null)
      {
        this.listaDeMaterial.push( this.utilesEscolares.at(i).get('nombre')?.value );

        // Lo convertimos a string para que muestre en la caja con slato de linea
        this.listaDeMaterialCadena = this.listaDeMaterial.join('\n')
        this.listaDeMaterialCadena = this.listaDeMaterialCadena.split("\n").join("<br>")
      }
      
    }

    console.log(this.listaDeMaterial);
    
  }
}
